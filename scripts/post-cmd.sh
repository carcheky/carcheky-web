#!/bin/bash

current_folder=$(pwd)
[ -f /usr/local/bin/ddev ] && ddev exec bash scripts/pre-cmd.sh && exit 0 
echo "----------------------------"
if [ "$current_folder" != "/var/www/html" ]; then
    echo "------- estás en PROD ------"
    chmod 444 sites/default/se*
    chmod 555 sites/default
    ./vendor/bin/drush deploy
else
    echo "------- estás en DEV -------"
fi
echo "----------------------------"
