#!/bin/bash
set -eux
bash scripts/pre-cmd.sh &> /dev/null
git pull
git reset --hard origin/production
php /usr/local/bin/composer2 install
# php ./vendor/bin/drupal-updater
